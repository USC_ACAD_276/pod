package finalproject.acad276.finalproject;

        import android.content.Intent;
        import android.graphics.Typeface;
        import android.provider.Settings;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.FrameLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.parse.FindCallback;
        import com.parse.FunctionCallback;
        import com.parse.GetCallback;
        import com.parse.Parse;
        import com.parse.ParseCloud;
        import com.parse.ParseException;
        import com.parse.ParseObject;
        import com.parse.ParseQuery;
        import com.parse.SaveCallback;

        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;

public class CreateProfile extends AppCompatActivity {

    EditText editName;
    EditText editPhone;
    EditText editEmail;
    EditText editAffiliation;
    FrameLayout continueButton;
    EditText editPassword;
    TextView continueText;
    TextView appBar;
    boolean userFound = false;
    String ID = "";
    String username ="";
    ArrayList<UserClass> objects = new ArrayList<UserClass>();
    String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        //declare everything
        editName = (EditText) findViewById(R.id.editName);
        editPhone = (EditText) findViewById(R.id.editPhone);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editAffiliation = (EditText) findViewById(R.id.editAffiliation);
        continueButton = (FrameLayout) findViewById(R.id.continueButton);
        editPassword = (EditText) findViewById(R.id.editPassword);
        continueText = (TextView) findViewById(R.id.continueText);
        appBar = (TextView) findViewById(R.id.appBar);

        //give everything a pretty font
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/AvenirNextCyr-Regular.otf");
        editName.setTypeface(type);
        editPhone.setTypeface(type);
        editEmail.setTypeface(type);
        editAffiliation.setTypeface(type);
        editPassword.setTypeface(type);
        continueText.setTypeface(type);
        appBar.setTypeface(type);

        //populates array with all Users on database
        refreshObjectList();

        //goes through array to see if user+password already exists and, if so, the objectId
        for (int i=0; i< objects.size();i++){
            if(android_id.equals(objects.get(i).getDevice())){
                userFound = true;
                ID = objects.get(i).getId();
                username = objects.get(i).getUsername();
            }
        }

        if (userFound == true){
            editName.setText(username);
            appBar.setText("UPDATE PROFILE");
            continueText.setText("UPDATE");
        }

        //when you click this button, all the shitty data is submitted
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //if User exists, update their details
                    if (userFound == true){

                        //TODO set the new user profile details

                    }

                    //if User doesn't exist, add them to database
                    else {
                        final HashMap<String, String> params = new HashMap<>();

                        params.put("full_name", editName.getText().toString());
                        params.put("email", editEmail.getText().toString());
                        params.put("phone_number", editPhone.getText().toString());
                        params.put("affiliation", editAffiliation.getText().toString());
                        params.put("password", editPassword.getText().toString());
                        params.put("device_number",android_id);

                        ParseCloud.callFunctionInBackground("createUser", params, new FunctionCallback<HashMap>() {
                            @Override
                            public void done(HashMap object, com.parse.ParseException e) {
                                Log.d("Result", object.toString());
                                Toast.makeText(getApplicationContext(), object.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    //when everything done go to MainActivity
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
            });

    }

    //method for populating array of Users on database
    private void refreshObjectList(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objectList, ParseException e) {
                if (e == null){
                    for (ParseObject object:objectList){
                        UserClass user = new UserClass(object.getObjectId(),object.getString("username"),object.getString("password"), object.getString("device_number"));
                        objects.add(user);
                    }

                }
                else {
                    Log.d(getClass().getSimpleName(), "Error: " + e.getMessage());
                }
            }
        });
    }

}