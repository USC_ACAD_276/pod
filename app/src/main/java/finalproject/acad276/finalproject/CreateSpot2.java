package finalproject.acad276.finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.HashMap;

public class CreateSpot2 extends AppCompatActivity {
    EditText editEmail;
    String EMAIL;
    FrameLayout continueButton;
    ImageView buttonAdd;
    ListView guest1;
    ArrayList<String> guestList;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_spot2);
        editEmail = (EditText) findViewById(R.id.editEmail);
        continueButton = (FrameLayout) findViewById(R.id.continueButton);
        buttonAdd = (ImageView) findViewById(R.id.buttonAdd);
        guest1 = (ListView)findViewById(R.id.guest1);
        guestList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, guestList);
        guest1.setAdapter(adapter);

        final HashMap<String, String> params = new HashMap<>();


        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opens MapsActivity
                Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(i);

            }
        });


        //button to add guests using emails to listview and update guestlist on Parse
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EMAIL = (editEmail.getText().toString());
                guestList.add(EMAIL);
//                Toast.makeText(getApplicationContext(), EMAIL, Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();

                //update each email to Parse
                //get device number
                String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                params.put("device_number", android_id);
                //put email from editText
                params.put("email", EMAIL);
                //use invitees function from Parse to update list of invited guests on eventTable
                ParseCloud.callFunctionInBackground("invitees", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String object, ParseException e) {
                        if (e != null) {
                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            return;
                        }
                        Log.d("Result", object);
                        Toast.makeText(getApplicationContext(), object, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}