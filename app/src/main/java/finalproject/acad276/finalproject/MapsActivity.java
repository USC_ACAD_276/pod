package finalproject.acad276.finalproject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //declare variables
    private GoogleMap mMap;
    FrameLayout continueButton;
    FrameLayout searchButton;
    TextView searchLabel;
    double doubleLatitude;
    double doubleLongitude;
    EditText searchText;
    Marker spotCenter;
    SeekBar seekBar;
    Circle circle;
    double radius;
    TextView radiusText;
    TextView continueText;
    TextView appBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        radiusText = (TextView) findViewById(R.id.radiusText);
        continueText = (TextView) findViewById(R.id.continueText);
        searchText = (EditText) findViewById(R.id.searchText);
        searchButton = (FrameLayout) findViewById(R.id.searchButton);
        searchLabel = (TextView) findViewById(R.id.searchLabel);
        appBar = (TextView) findViewById(R.id.appBar);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/AvenirNextCyr-Regular.otf");
        continueText.setTypeface(type);
        searchText.setTypeface(type);
        searchLabel.setTypeface(type);
        radiusText.setTypeface(type);
        appBar.setTypeface(type);

        //set seekBar progress to determine radius of spot
        radiusText.setText("RADIUS");
        seekBar.setProgress(0);
        radius = 0;
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                radius = progress;
                circle.setRadius(progress);
                radiusText.setText(""+radius+"m");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        //call getLocationFromAddress method
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocationFromAddress(searchText.getText().toString());
            }
        });


        //create spot
        continueButton = (FrameLayout) findViewById(R.id.continueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //putting in location
                final HashMap<String, String> params = new HashMap<>();
                //get device number
                String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                params.put("device_number", android_id);
                //inputting longitude, latitude and radius
                params.put("longitude",String.valueOf(spotCenter.getPosition().longitude));
                params.put("latitude", String.valueOf(spotCenter.getPosition().latitude));
                params.put("radius", String.valueOf(radius));
                //carry out location function
                ParseCloud.callFunctionInBackground("location", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String object, ParseException e) {
                        if (e != null) {
                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            return;
                        }
                        Log.d("Result", object);
                        Toast.makeText(getApplicationContext(), object, Toast.LENGTH_SHORT).show();
                    }
                });



                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                //send latitude, longitude, radius to parse
            }
        });
    }

    //method for getting longitude and latitude from a search query
    public void getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address != null) {
                Address location = address.get(0);
                location.getLatitude();
                location.getLongitude();
                String addressLine = location.getAddressLine(0);

                //move the marker to the new location
                LatLng here2 = new LatLng(location.getLatitude(), location.getLongitude());
                spotCenter.setPosition(here2);
                spotCenter.setTitle(addressLine);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here2, 18.0f));
                circle.setCenter(here2);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        GPSTracker gpsTracker = new GPSTracker(this);

        //sets default marker to user's current location
        doubleLatitude = Double.parseDouble(String.valueOf(gpsTracker.latitude));
        doubleLongitude = Double.parseDouble(String.valueOf(gpsTracker.longitude));
        String addressLine = gpsTracker.getAddressLine(this);
        LatLng here = new LatLng(doubleLatitude,doubleLongitude);
        spotCenter = mMap.addMarker(new MarkerOptions()
                .position(here)
                .title(addressLine)
                .draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 18.0f));
        circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(doubleLatitude, doubleLongitude))
                .radius(radius)
                .strokeWidth(5)
                .strokeColor(R.color.colorAccent)
                .fillColor(0x10240256));
        setMarkerDragListener(mMap);
    }

    private void setMarkerDragListener(GoogleMap map) {
        map.setOnMarkerDragListener(new OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                marker.setTitle("" + marker.getPosition().latitude + " , " + marker.getPosition().longitude);
                circle.setCenter(marker.getPosition());
            }
        });
    }

}