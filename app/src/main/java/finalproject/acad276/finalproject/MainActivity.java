package finalproject.acad276.finalproject;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    ImageView add;
    ImageView prof;
    TextView date1big;
    TextView date1small;
    TextView date2big;
    TextView date2small;
    TextView date3big;
    TextView date3small;
    TextView spotName1;
    TextView spotName2;
    TextView spotName3;
    TextView spotDetails1;
    TextView spotDetails2;
    TextView spotDetails3;
    Switch switchEvent;
    String toggleStatus;
    double lat2;
    double lng2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        add = (ImageView) findViewById(R.id.add);
        prof = (ImageView) findViewById(R.id.prof);

        date1big = (TextView) findViewById(R.id.date1big);
        date1small = (TextView) findViewById(R.id.date1small);
        date2big = (TextView) findViewById(R.id.date2big);
        date2small = (TextView) findViewById(R.id.date2small);
        date3big = (TextView) findViewById(R.id.date3big);
        date3small = (TextView) findViewById(R.id.date3small);
        spotName1 = (TextView) findViewById(R.id.spotName1);
        spotName2 = (TextView) findViewById(R.id.spotName2);
        spotName3 = (TextView) findViewById(R.id.spotName3);
        spotDetails1 = (TextView) findViewById(R.id.spotDetails1);
        spotDetails2 = (TextView) findViewById(R.id.spotDetails2);
        spotDetails3 = (TextView) findViewById(R.id.spotDetails3);
        switchEvent = (Switch) findViewById(R.id.switchEvent);



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CreateSpot1.class);
                startActivity(i);
            }
        });

        prof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CreateProfile.class);
                startActivity(i);
            }
        });

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/AvenirNextCyr-Regular.otf");
        date1big.setTypeface(type);
        date2big.setTypeface(type);
        date3big.setTypeface(type);
        date1small.setTypeface(type);
        date2small.setTypeface(type);
        date3small.setTypeface(type);

        spotName1.setTypeface(type);
        spotName2.setTypeface(type);
        spotName3.setTypeface(type);
        spotDetails1.setTypeface(type);
        spotDetails2.setTypeface(type);
        spotDetails3.setTypeface(type);


        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        Toast.makeText(getApplicationContext(), android_id, Toast.LENGTH_SHORT).show();
        final HashMap<String, String> params = new HashMap<>();
        params.put("device_number", android_id);
        //carry out function
        ParseCloud.callFunctionInBackground("loadCalendarInvited", params, new FunctionCallback<HashMap>() {
            @Override
            public void done(HashMap object, com.parse.ParseException e) {
                //print error message
                if (e != null) {
                    Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    return;
                }

                //make a Toast showing all info in the line of Parse
                Log.d("Show", object.toString());
                List<String> eventInvite = Collections.list(Collections.enumeration(object.values()));
                String date = eventInvite.get(1);
                spotDetails2.setText("" + eventInvite.get(0) + " - " + eventInvite.get(2));
                date2big.setText("" + date.charAt(3) + date.charAt(4));
                spotName2.setText(eventInvite.get(3));
//                Toast.makeText(getApplicationContext(), object.toString(), Toast.LENGTH_SHORT).show();


            }
        });
        ParseCloud.callFunctionInBackground("loadCalendarHosting", params, new FunctionCallback<HashMap>() {
            @Override
            public void done(HashMap object, com.parse.ParseException e) {
                //print error message
                if (e != null) {
                    Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    return;
                }

                //make a Toast showing all info in the line of Parse
                Log.d("Show", object.toString());
//                Toast.makeText(getApplicationContext(), object.toString(), Toast.LENGTH_SHORT).show();
                List<String> valueList = Collections.list(Collections.enumeration(object.values()));
//                Toast.makeText(getApplicationContext(), valueList.get(0), Toast.LENGTH_SHORT).show();
                String date = valueList.get(1);

                spotDetails1.setText(valueList.get(0)+" - "+valueList.get(1));
                date1big.setText(""+date.charAt(3)+date.charAt(4));
                spotName1.setText(valueList.get(3));

            }
        });


//toggle
        switchEvent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final HashMap<String, String> params = new HashMap<>();
                String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                params.put("device_number", android_id);
                if (isChecked) {
                    // The toggle is enabled
                    toggleStatus = "ON";
//                    Toast.makeText(getApplicationContext(), "Cheese", Toast.LENGTH_SHORT).show();

                } else {
                    // The toggle is disabled
                    toggleStatus = "OFF";
//                    Toast.makeText(getApplicationContext(), "Broccoli", Toast.LENGTH_SHORT).show();
                }

                params.put("activate", toggleStatus);
                ParseCloud.callFunctionInBackground("activate", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String object, ParseException e) {
                        if (e != null) {
                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            return;
                        }
                        Log.d("Result", object);
                        Toast.makeText(getApplicationContext(), object, Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });




        GPSTracker gpsTracker = new GPSTracker(this);
        lat2 = Double.parseDouble(String.valueOf(gpsTracker.latitude));
        lng2 = Double.parseDouble(String.valueOf(gpsTracker.longitude));


        //  Testing getting back radius, long, lat from on event
        //give your device ID

        params.put("device_number", android_id);

        ParseCloud.callFunctionInBackground("checkForActivate", params, new FunctionCallback<HashMap>() {
            @Override
            public void done(HashMap object, ParseException e) {
                if (e != null) {
                    Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    return;
                }

                Log.d("Result", object.toString());
                Toast.makeText(getApplicationContext(), object.toString(), Toast.LENGTH_SHORT).show();
                List<String> onEventList = Collections.list(Collections.enumeration(object.values()));
                Toast.makeText(getApplicationContext(), onEventList.get(0), Toast.LENGTH_SHORT).show();
                String latId = onEventList.get(0);
                String longId= onEventList.get(1);
                String radId = onEventList.get(2);
                Double lat1 = Double.parseDouble(latId);
                Double long1 = Double.parseDouble(longId);
                Double rad = Double.parseDouble(radId);

                double distance = distFrom(lat1, long1,lat2,lng2);

                if(rad >= distance){
                    //check-in to event
                    String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    params.put("device_number", android_id);
                    params.put("latitude", latId);
                    ParseCloud.callFunctionInBackground("checkIn", params, new FunctionCallback<String>() {
                        @Override
                        public void done(String object, ParseException e) {
                            if (e != null) {
                                Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                return;
                            }

                            Log.d("Result", object);
                            Toast.makeText(getApplicationContext(), object, Toast.LENGTH_SHORT).show();

                        }

                    });

                    params.put("device_number", android_id);
                    ParseCloud.callFunctionInBackground("attendedEvent", params, new FunctionCallback<HashMap>() {
                        @Override
                        public void done(HashMap object, ParseException e) {
                            if(e != null){
                                Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                return;
                            }

                            Log.d("Result",object.toString());
                            Toast.makeText(getApplicationContext(),object.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }


            }

        });


    }

    //method for finding distance between spot latlng and current latlng
    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = (double) (earthRadius * c);

        return dist;
    }
}