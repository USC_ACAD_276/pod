package finalproject.acad276.finalproject;

    import android.content.Intent;
    import android.graphics.Typeface;
    import android.provider.Settings;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.util.Log;
    import android.view.View;
    import android.widget.ArrayAdapter;
    import android.widget.EditText;
    import android.widget.FrameLayout;
    import android.widget.ImageView;
    import android.widget.ListView;
    import android.widget.TextView;
    import android.widget.Toast;

    import com.parse.FunctionCallback;
    import com.parse.Parse;
    import com.parse.ParseCloud;
    import com.parse.ParseException;

    import java.util.ArrayList;
    import java.util.HashMap;

public class CreateSpot1 extends AppCompatActivity {

    //declare everything
    EditText editSpot;
    EditText editHost;
    EditText editDate;
    EditText editTime;
    FrameLayout continueButton;
    TextView continueText;
    TextView appBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_spot1);

        editDate = (EditText)findViewById(R.id.editDate);
        editSpot = (EditText)findViewById(R.id.editSpot);
        editHost = (EditText)findViewById(R.id.editHost);
        editTime = (EditText)findViewById(R.id.editTime);
        continueButton = (FrameLayout)findViewById(R.id.continueButton);
        continueText = (TextView) findViewById(R.id.continueText);
        appBar = (TextView) findViewById(R.id.appBar);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/AvenirNextCyr-Regular.otf");
        editDate.setTypeface(type);
        editSpot.setTypeface(type);
        editHost.setTypeface(type);
        editTime.setTypeface(type);
        continueText.setTypeface(type);
        appBar.setTypeface(type);



//submit button to save inputted information for creation of spot
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final HashMap<String, String> params = new HashMap<>();
                String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                params.put("device_number",android_id);
                params.put("event_name", editSpot.getText().toString());
                params.put("host_name", editHost.getText().toString());
                params.put("event_date", editDate.getText().toString());
                params.put("event_time", editTime.getText().toString());


                ParseCloud.callFunctionInBackground("createSpot", params, new FunctionCallback<HashMap>() {
                    @Override
                    public void done(HashMap object, ParseException e) {
                        if (e != null) {
                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            return;
                        }

                        Log.d("Result", object.toString());
                        Toast.makeText(getApplicationContext(), object.toString(), Toast.LENGTH_SHORT).show();

                    }

                });

                //opens MapsActivity
                Intent i = new Intent(getApplicationContext(), CreateSpot2.class);
                startActivity(i);

            }
        });

    }


}
