package finalproject.acad276.finalproject;

/**
 * Created by janebyon on 12/10/15.
 */
public class UserClass {

    private String id;
    private String username;
    private String password;
    private String device;

    UserClass(String userId, String userUsername, String userPassword, String userDevice){
        id = userId;
        username = userUsername;
        password = userPassword;
        device = userDevice;
    }

    public String getId(){
        return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getPassword(){
        return password;
    }
    public void setPassword(){
        this.password = password;
    }

    public void setDevice(){
        this.device = device;
    }
    public String getDevice(){
        return device;
    }

    @Override
    public String toString(){
        return this.getId();
    }

}
